#!/bin/bash
# sh -c "$(curl -fsSL https://url.com/repo/master/setup.sh)"

sudo pacman -Sy git python ansible sshpass

cd /tmp
git clone https://gitlab.com/RValeye/ansible-manjaro-i3.git
cd ansible-manjaro-i3

ansible-playbook -K playbook.yml
