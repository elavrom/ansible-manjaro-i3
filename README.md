# ansible-manjaro-i3

Personal ansible configuration for my computer.
Based on [this guy's project](https://github.com/0b11stan/ansible-yseult).

## Installation

After having installed [Manjaro's i3 OS](https://manjaro.org/download/community/i3/), Simply run:

```
$ sh -c "$(curl -fsSL https://gitlab.com/RValeye/ansible-manjaro-i3/-/raw/master/setup.sh)"
```

This command will install needed packages (git, python, ansible, sshpass) and then launch the playbook.