CMD=ansible-playbook -K playbook.yml

check:
	$(CMD) --check --diff

apply:
	$(CMD)
